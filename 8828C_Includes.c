void lift(int speed)
{
	motor[liftLeft] = speed;
	motor[liftRight] = speed;
}
void move (int leftSpeed, int rightSpeed)
{
	motor[frontRight] = rightSpeed;
	motor[frontLeft] = leftSpeed;
	motor[backRight] = rightSpeed;
	motor[backLeft] = leftSpeed;
}

//for autonomous
void moveArm (int speed)
{
	motor[clawArm] = speed;
}

//
void armLimiter(int armSpeed)
{
	if (armSpeed > 0 && SensorValue[armPosition] > -600)
	{
		motor[clawArm] = armSpeed;
	}
	else if (armSpeed <= 0 && SensorValue[armPosition] < 750)
	{
		motor[clawArm] = armSpeed;
	}
	else
	{
		motor[clawArm] = 0;
	}
}

void mobileGoalLift()
{
	if (vexRT[Btn8U] == 1 && nMotorEncoder[mobileGoal] <= 800)
	{
		motor[mobileGoal] = 127;
	}
	else if(vexRT[Btn8D] == 1 && nMotorEncoder(mobileGoal) >= 0)
	{
		motor[mobileGoal] = -100;
	}
	else
	{
		motor[mobileGoal] = 0;
	}
}

//lift for driver control
void controlledLift()
{
	if (vexRT[Btn5U] == 1 && nMotorEncoder(liftLeft) < 1100)
	{
		//sets maximum value of encoder, be wary of physical stop
		lift(127);
		SensorValue[leftLED] = false;
		SensorValue[rightLED] = true;
	}

	else if (vexRT[Btn5D] == 1 && nMotorEncoder(liftLeft) > 30)
	{
		//sets minimum value of encoder, should be greater than 0 b/c of physical stop
		lift(-100);
		SensorValue[leftLED] = false;
		SensorValue[rightLED] = true;
	}
	else if (vexRT[Btn5D] == 1 || vexRT[Btn5U] == 1) //if user is attempting to press a button and it is at max, turn in left LED.
	{
		lift(0);
		SensorValue[leftLED] = true;
		SensorValue[rightLED] = true;

	}
	else //if no buttons are pressed, set lift to 0, and turn LED's off.
	{
		lift(0);
		SensorValue[leftLED] = false;
		SensorValue[rightLED] = false;
	}
}

void controlledClawHand()
{
	if(vexRT[Btn6D] == 1)
	{
		motor[clawHand] = -40;
	}
	else if(vexRT[Btn6U] == 1)
	{
		motor[clawHand] = 40;
	}
	else
	{
		motor[clawHand] = 0;
	}
}
